package com.gitlab.jantolis.chatroom.receiver;

import com.gitlab.jantolis.chatroom.BasicChatMessage;
import com.gitlab.jantolis.chatroom.BasicUser;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import com.gitlab.jantolis.chatroom.api.message.content.GeoContent;
import com.gitlab.jantolis.chatroom.api.message.content.ImageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import com.gitlab.jantolis.chatroom.endpoints.Type;
import com.gitlab.jantolis.chatroom.endpoints.responses.Location;
import com.gitlab.jantolis.chatroom.endpoints.responses.Message;
import com.gitlab.jantolis.chatroom.endpoints.responses.Subscriber;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

public class MessageToContentMapper {

    public static final String UNSUPPORTED_MESSAGE_TYPE = "Failed to map Message of unsupported type ";

    private final Message message;
    private final Subscriber subscriber;

    public MessageToContentMapper(Message message, Subscriber subscriber) {
        this.message = message;
        this.subscriber = subscriber;
    }

    public BasicChatMessage get() {
        List<Supplier<Optional<MessageContent>>> mappers = new LinkedList<>();
        mappers.add(this::mapToTextContent);
        mappers.add(this::mapToImageContent);
        mappers.add(this::mapToGeoContent);

        MessageContent messageContent = mappers.stream()
                .map(Supplier::get)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(UNSUPPORTED_MESSAGE_TYPE + message));

        return new BasicChatMessage(
                message.getId(),
                messageContent,
                mapToUser(message.getConversationId()),
                message.getSentAt()
        );
    }

    private Optional<MessageContent> mapToGeoContent() {
        return Optional.of(message)
                .filter(msg -> Type.MAP.equals(msg.getType()))
                .filter(msg -> msg.getText().isPresent())
                .filter(msg -> msg.getLocation().isPresent())
                .map(msg -> {
                    Location location = msg.getLocation().get();
                    String address = msg.getText().get();
                    return new GeoContent(location.getLatitude(), location.getLongitude(), address);
                });
    }

    private Optional<MessageContent> mapToImageContent() {
        return Optional.of(message)
                .filter(msg -> Type.IMAGE.equals(msg.getType()))
                .flatMap(Message::getUrl)
                .map(ImageContent::new);
    }

    private Optional<MessageContent> mapToTextContent() {
        return Optional.of(message)
                .filter(msg -> Type.TEXT.equals(msg.getType()))
                .flatMap(Message::getText)
                .map(TextContent::new);
    }

    private BasicUser mapToUser(String conversationId) {
        return new BasicUser(
                subscriber.getId(),
                conversationId,
                subscriber.getFirstName() + " " + subscriber.getLastName()
        );
    }
}
