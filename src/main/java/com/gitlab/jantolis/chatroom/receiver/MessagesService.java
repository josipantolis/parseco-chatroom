package com.gitlab.jantolis.chatroom.receiver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.jantolis.chatroom.BasicChatMessage;
import com.gitlab.jantolis.chatroom.ChatMessageMetaData;
import com.gitlab.jantolis.chatroom.ThreadSafeChatRoom;
import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import com.gitlab.jantolis.chatroom.endpoints.Mappable;
import com.gitlab.jantolis.chatroom.endpoints.MessagesEndpoint;
import com.gitlab.jantolis.chatroom.endpoints.responses.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rx.Observable;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Service
public class MessagesService {

    private final ObjectMapper objectMapper;
    private final MessagesEndpoint messagesEndpoint;

    private final ThreadSafeChatRoom chatRoomContext;

    @Autowired
    public MessagesService(
            ObjectMapper objectMapper,
            MessagesEndpoint messagesEndpoint,
            ThreadSafeChatRoom chatRoomContext) {
        this.objectMapper = objectMapper;
        this.messagesEndpoint = messagesEndpoint;
        this.chatRoomContext = chatRoomContext;
    }

    public Observable<? extends ChatMessage> fetchUnreadFor(Conversation conversation) {
        Subscriber subscriber = conversation.getSubscriber();

        return messagesEndpoint.getMessagesFrom(
                conversation.getId(),
                getFetchFrom(conversation))
                .compose(this::unwrap)
                .doOnNext(message -> chatRoomContext.registerMessageFor(new ChatMessageMetaData(
                        message.getId(),
                        message.getSentAt()
                ), subscriber.getId()))
                .filter(message -> Sender.USER.equals(message.getSender()))
                .filter(message -> message.getText() != null)
                .map(message -> new MessageToContentMapper(message, subscriber))
                .map(MessageToContentMapper::get)
                .onExceptionResumeNext(Observable.<BasicChatMessage>empty());
    }


    private Observable<Message> unwrap(Observable<CollectionResponse<Messages>> source) {
        return source
                .map(CollectionResponse::getUnwrapped)
                .map(Messages::getMessages)
                .flatMap(Observable::from);

    }

    private Mappable<Instant> getFetchFrom(Conversation conversation) {
        return new Mappable<>(conversation.getPreviewMessage()
                .getSentAt()
                .minus(1, ChronoUnit.SECONDS),
                objectMapper
        );
    }
}