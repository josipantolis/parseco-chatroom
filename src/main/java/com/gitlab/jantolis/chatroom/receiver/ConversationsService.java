package com.gitlab.jantolis.chatroom.receiver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.jantolis.chatroom.BasicUser;
import com.gitlab.jantolis.chatroom.ThreadSafeChatRoom;
import com.gitlab.jantolis.chatroom.endpoints.ConversationsEndpoint;
import com.gitlab.jantolis.chatroom.endpoints.Mappable;
import com.gitlab.jantolis.chatroom.endpoints.responses.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rx.Observable;

import java.time.Instant;

@Service
public class ConversationsService {

    private final ObjectMapper objectMapper;
    private final ConversationsEndpoint conversationsEndpoint;
    private final ThreadSafeChatRoom chatRoomContext;

    @Autowired
    public ConversationsService(
            ConversationsEndpoint conversationsEndpoint,
            ObjectMapper objectMapper,
            ThreadSafeChatRoom chatRoomContext) {
        this.conversationsEndpoint = conversationsEndpoint;
        this.objectMapper = objectMapper;
        this.chatRoomContext = chatRoomContext;
    }

    public Observable<Conversation> fetchAll() {
        return conversationsEndpoint.getConversationsFrom(null)
                .map(CollectionResponse::getUnwrapped)
                .map(Conversations::getConversations)
                .flatMap(Observable::from)
                .doOnNext(this::registerWithContext)
                .onExceptionResumeNext(Observable.<Conversation>empty());
    }


    public Observable<Conversation> fetchNewerThan(Instant instant) {
        return conversationsEndpoint.getConversationsFrom(new Mappable<>(instant, objectMapper))
                .map(CollectionResponse::getUnwrapped)
                .map(Conversations::getConversations)
                .flatMap(Observable::from)
                .filter(conversation -> conversation.getPreviewMessage() != null)
                .filter(conversation -> Sender.USER.equals(conversation.getPreviewMessage().getSender()))
                .filter(conversation -> !conversation.getPreviewMessage().getRead())
                .doOnNext(this::registerWithContext)
                .onExceptionResumeNext(Observable.<Conversation>empty());
    }

    private void registerWithContext(Conversation conversation) {
        chatRoomContext.registerUser(mapToUser(conversation));
    }

    private BasicUser mapToUser(Conversation conversation) {
        Subscriber subscriber = conversation.getSubscriber();
        return new BasicUser(
                subscriber.getId(),
                conversation.getId(),
                subscriber.getFirstName() + " " + subscriber.getLastName()
        );
    }
}
