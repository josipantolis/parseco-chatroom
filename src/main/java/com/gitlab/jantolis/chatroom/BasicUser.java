package com.gitlab.jantolis.chatroom;

import com.gitlab.jantolis.chatroom.api.User;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Optional;

@ToString
@EqualsAndHashCode(of = "id")
public class BasicUser implements User {

    private final String id;
    private final String conversationId;
    private String screenName;
    private ChatMessageMetaData latestMessage;

    public BasicUser(
            String id,
            String conversationId,
            String screenName) {
        this.id = id;
        this.conversationId = conversationId;
        this.screenName = screenName;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getConversationId() {
        return conversationId;
    }

    @Override
    public String getScreenName() {
        return screenName;
    }

    @Override
    public void setScreenName(String newScreenName) {
        this.screenName = newScreenName;
    }

    @Override
    public Optional<String> getLastReceivedMessageId() {
        return Optional.ofNullable(latestMessage)
                .map(ChatMessageMetaData::id);
    }

    public BasicUser registerMessageReceipt(ChatMessageMetaData message) {
        latestMessage = Optional.ofNullable(latestMessage)
                .filter(msg -> msg.getSentAt().isBefore(message.getSentAt()))
                .orElse(message);
        return this;
    }


}
