package com.gitlab.jantolis.chatroom.sender;

import com.gitlab.jantolis.chatroom.api.message.ContentMapper;
import com.gitlab.jantolis.chatroom.api.message.content.GeoContent;
import com.gitlab.jantolis.chatroom.api.message.content.ImageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import com.gitlab.jantolis.chatroom.endpoints.requests.Reply;

public class ContentToReplyMapper implements ContentMapper<Reply> {

    @Override
    public Reply map(TextContent textContent) {
        return Reply.withText(textContent.getText());
    }

    @Override
    public Reply map(GeoContent geoContent) {
        return Reply.withLocation(
                geoContent.getAddress(),
                geoContent.getLatitude(),
                geoContent.getLongitude()
        );
    }

    @Override
    public Reply map(ImageContent imageContent) {
        return Reply.withImage(imageContent.getImageUrl());
    }
}
