package com.gitlab.jantolis.chatroom.sender;

import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.ResponseInterceptor;
import com.gitlab.jantolis.chatroom.api.User;
import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;

import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class BasicResponseContext implements ResponseContext {

    private final ChatMessage originalMessage;
    private final Iterator<ResponseInterceptor> nextInterceptor;
    private final List<UnaryOperator<Observable<MessageContent>>> processors = new LinkedList<>();
    private final List<Predicate<User>> filters = new LinkedList<>();

    private final static Logger logger = LoggerFactory.getLogger(BasicResponseContext.class.getName());

    public BasicResponseContext(ChatMessage originalMessage, List<ResponseInterceptor> interceptors) {
        this.originalMessage = originalMessage;
        this.nextInterceptor = interceptors.stream()
                .sorted(Comparator.comparing(ResponseInterceptor::getOrder))
                .iterator();

        next();
    }

    @Override
    public ChatMessage getOriginalMessage() {
        return originalMessage;
    }

    @Override
    public void addContentProcessor(UnaryOperator<Observable<MessageContent>> processor) {
        this.processors.add(processor);
    }

    @Override
    public void addRecipientFilter(Predicate<User> filter) {
        this.filters.add(filter);
    }

    public Observable<MessageContent> generateResponseContent() {
        return processors.stream()
                .reduce((op1, op2) -> observable -> op2.apply(op1.apply(observable)))
                .orElse(UnaryOperator.identity())
                .apply(Observable.just(originalMessage.getContent()));
    }

    public boolean shouldReceive(User potentialRecipient) {
        return filters.stream()
                .reduce(Predicate::and)
                .orElse(user -> true)
                .test(potentialRecipient);
    }

    @Override
    public void next() {
        while (nextInterceptor.hasNext()) {
            ResponseInterceptor interceptor = nextInterceptor.next();
            try {
                if (interceptor.isApplicableTo(originalMessage)) {
                    interceptor.intercept(this);
                    return;
                }
            } catch (RuntimeException regrettable) {
                logger.error("Exception calling interceptor " + interceptor.getClass().getName(), regrettable);
            }
        }
    }
}
