package com.gitlab.jantolis.chatroom.sender;

import com.gitlab.jantolis.chatroom.ChatMessageMetaData;
import com.gitlab.jantolis.chatroom.Pair;
import com.gitlab.jantolis.chatroom.ThreadSafeChatRoom;
import com.gitlab.jantolis.chatroom.api.ResponseInterceptor;
import com.gitlab.jantolis.chatroom.api.User;
import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import com.gitlab.jantolis.chatroom.endpoints.MessagesEndpoint;
import com.gitlab.jantolis.chatroom.endpoints.requests.Reply;
import com.gitlab.jantolis.chatroom.endpoints.responses.CollectionResponse;
import com.gitlab.jantolis.chatroom.endpoints.responses.Message;
import com.gitlab.jantolis.chatroom.endpoints.responses.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rx.Observable;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toSet;

@Service
public class SenderService {

    public static final String UNINITIALIZED_USER = "Failed to send message to user without last received message ";

    private final MessagesEndpoint messagesEndpoint;
    private final List<ResponseInterceptor> responseInterceptors;

    private final ThreadSafeChatRoom chatRoomContext;

    @Autowired
    public SenderService(
            MessagesEndpoint messagesEndpoint,
            List<ResponseInterceptor> responseInterceptors,
            ThreadSafeChatRoom chatRoomContext) {
        this.messagesEndpoint = messagesEndpoint;
        this.responseInterceptors = responseInterceptors;
        this.chatRoomContext = chatRoomContext;
    }

    public Observable<Void> sendMessage(ChatMessage originalMessage) {
        BasicResponseContext responseContext = new BasicResponseContext(originalMessage, responseInterceptors);
        Set<User> usersToRespondTo = chatRoomContext.getAllUsers().stream()
                .filter(user -> user.getLastReceivedMessageId().isPresent())
                .filter(responseContext::shouldReceive)
                .collect(toSet());

        return responseContext.generateResponseContent()
                .map(messageContent -> messageContent.mapWith(new ContentToReplyMapper()))
                .flatMapIterable(reply -> usersToRespondTo.stream()
                        .map(user -> new Pair<>(user, reply))
                        .collect(Collectors.toSet())
                ).flatMap(pair -> {
                    User user = pair.getKey();
                    Reply reply = pair.getValue();
                    String messageId = user.getLastReceivedMessageId()
                            .orElseThrow(() -> new IllegalStateException(UNINITIALIZED_USER + user));
                    return messagesEndpoint.sendMessage(user.getConversationId(), messageId, reply)
                            .compose(this::unwrap)
                            .doOnNext(message -> chatRoomContext.registerMessageFor(
                                    new ChatMessageMetaData(
                                            message.getId(),
                                            message.getSentAt()
                                    ),
                                    user.getId())
                            );
                }).onExceptionResumeNext(Observable.<Message>empty())
                .map(message -> null);
    }

    private Observable<Message> unwrap(Observable<CollectionResponse<Messages>> source) {
        return source
                .map(CollectionResponse::getUnwrapped)
                .map(Messages::getMessages)
                .flatMap(Observable::from);

    }
}
