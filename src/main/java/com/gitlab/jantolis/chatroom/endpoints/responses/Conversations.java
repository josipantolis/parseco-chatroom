package com.gitlab.jantolis.chatroom.endpoints.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

import java.util.Collection;
import java.util.Optional;

import static java.util.Collections.emptyList;

@Getter
@ToString
public class Conversations {

    private final Collection<Conversation> conversations;

    @JsonCreator
    public Conversations(@JsonProperty("conversations") Collection<Conversation> conversations) {
        this.conversations = Optional.ofNullable(conversations).orElse(emptyList());
    }
}
