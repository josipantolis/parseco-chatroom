package com.gitlab.jantolis.chatroom.endpoints;

import com.gitlab.jantolis.chatroom.endpoints.requests.Invitation;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

public interface InviteEndpoint {

    @POST("channels/{channelId}/invite")
    Observable<Void> sendInvitation(
            @Path("channelId") String channelId,
            @Body Invitation invitation
    );
}
