package com.gitlab.jantolis.chatroom.endpoints.responses;

import com.fasterxml.jackson.annotation.JsonCreator;

import static java.util.Arrays.stream;

public enum Status {

    AVAILABLE("available"),
    UNAVAILABLE("unavailable");

    private final String name;

    Status(String name) {
        this.name = name;
    }

    @JsonCreator
    public static Status fromName(final String name) {
        return stream(Status.values())
                .filter(status -> status.name.equals(name))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No Status found for name " + name));
    }
}
