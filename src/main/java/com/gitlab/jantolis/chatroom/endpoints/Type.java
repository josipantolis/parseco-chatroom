package com.gitlab.jantolis.chatroom.endpoints;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import static java.util.Arrays.stream;

public enum Type {

    TEXT("text"),
    IMAGE("image"),
    VIDEO("video"),
    URL("url"),
    MAP("map");

    private final String name;

    Type(String name) {
        this.name = name;
    }

    @JsonCreator
    public static Type fromName(final String name) {
        return stream(Type.values())
                .filter(type -> type.name.equals(name))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No Type found for name " + name));
    }

    @JsonValue
    public String getName() {
        return name;
    }
}
