package com.gitlab.jantolis.chatroom.endpoints.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class CollectionResponse<Resource> {

    private final Resource unwrapped;

    @JsonCreator
    public CollectionResponse(@JsonProperty("_embedded") Resource unwrapped) {
        this.unwrapped = unwrapped;
    }
}
