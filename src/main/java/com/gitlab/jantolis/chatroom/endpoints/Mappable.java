package com.gitlab.jantolis.chatroom.endpoints;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Mappable<T> {

    private final T instant;
    private final ObjectMapper mapper;

    public Mappable(T instant, ObjectMapper mapper) {
        this.instant = instant;
        this.mapper = mapper;
    }

    @Override
    public String toString() {
        try {
            return mapper.writeValueAsString(instant).replaceAll("\"", "");
        } catch (JsonProcessingException e) {
            return null;
        }
    }
}
