package com.gitlab.jantolis.chatroom.endpoints;

import com.gitlab.jantolis.chatroom.endpoints.requests.Reply;
import com.gitlab.jantolis.chatroom.endpoints.responses.CollectionResponse;
import com.gitlab.jantolis.chatroom.endpoints.responses.Messages;
import retrofit2.http.*;
import rx.Observable;

import java.time.Instant;

public interface MessagesEndpoint {

    @GET("conversations/{conversationId}/messages")
    Observable<CollectionResponse<Messages>> getMessagesFrom(
            @Path("conversationId") String conversationId,
            @Query("sentFrom") Mappable<Instant> from);

    @POST("conversations/{conversationId}/messages/{messageId}/reply")
    Observable<CollectionResponse<Messages>> sendMessage(
            @Path("conversationId") String conversationId,
            @Path("messageId") String messageId,
            @Body Reply reply
    );

}
