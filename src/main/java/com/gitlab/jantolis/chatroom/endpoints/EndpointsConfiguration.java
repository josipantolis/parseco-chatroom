package com.gitlab.jantolis.chatroom.endpoints;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.jantolis.chatroom.ParsecoConfiguration;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Configuration
@EnableConfigurationProperties(ParsecoConfiguration.class)
public class EndpointsConfiguration {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ParsecoConfiguration parsecoConfiguration;

    @Bean
    public OkHttpClient okHttpClient() {
        Logger logger = LoggerFactory.getLogger("Retrofit");
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(logger::info);
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(chain -> {
                    Request request = chain.request()
                            .newBuilder()
                            .addHeader(
                                    "Authorization",
                                    parsecoConfiguration.getApi().getAuthorization()
                            ).build();
                    return chain.proceed(request);
                }).build();
    }

    @Bean
    public Retrofit retrofit() {
        return new Retrofit.Builder()
                .baseUrl(parsecoConfiguration.getApi().getBase())
                .client(okHttpClient())
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @Bean
    public ConversationsEndpoint conversationsEndpoint() {
        return retrofit().create(ConversationsEndpoint.class);
    }

    @Bean
    public MessagesEndpoint messagesEndpoint() {
        return retrofit().create(MessagesEndpoint.class);
    }

    @Bean
    public InviteEndpoint inviteEndpoint() {
        return retrofit().create(InviteEndpoint.class);
    }
}
