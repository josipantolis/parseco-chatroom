package com.gitlab.jantolis.chatroom.endpoints.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Invitation {

    @JsonProperty("msisdns")
    private final String msisdns;

    @JsonProperty("message")
    private final String message;

    @JsonProperty("transliterationKey")
    private final String transliterationKey = "-1";

    public Invitation(String phoneNumber, String message) {
        this.msisdns = phoneNumber;
        this.message = message;
    }
}
