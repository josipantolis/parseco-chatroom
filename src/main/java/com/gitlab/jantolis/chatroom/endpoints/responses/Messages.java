package com.gitlab.jantolis.chatroom.endpoints.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

import java.util.Collection;

@Getter
@ToString
public class Messages {

    private final Collection<Message> messages;

    @JsonCreator
    public Messages(@JsonProperty("messages") Collection<Message> messages) {
        this.messages = messages;
    }
}
