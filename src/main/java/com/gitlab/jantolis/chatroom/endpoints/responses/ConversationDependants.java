package com.gitlab.jantolis.chatroom.endpoints.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ConversationDependants {

    private final Subscriber subscriber;
    private final Message previewMessage;

    @JsonCreator
    public ConversationDependants(
            @JsonProperty("subscriber") Subscriber subscriber,
            @JsonProperty("message") Message previewMessage) {
        this.subscriber = subscriber;
        this.previewMessage = previewMessage;
    }
}
