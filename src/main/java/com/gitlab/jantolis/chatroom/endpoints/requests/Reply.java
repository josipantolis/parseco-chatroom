package com.gitlab.jantolis.chatroom.endpoints.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gitlab.jantolis.chatroom.endpoints.Type;
import com.gitlab.jantolis.chatroom.endpoints.responses.Location;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Reply {

    @JsonProperty("text")
    private final String text;

    @JsonProperty("url")
    private final String url;

    @JsonProperty("location")
    private final Location location;

    @JsonProperty("type")
    private final Type type;

    public Reply(String text, String url, Location location, Type type) {
        this.text = text;
        this.url = url;
        this.location = location;
        this.type = type;
    }

    public static Reply withText(String text) {
        return new Reply(text, null, null, Type.TEXT);
    }

    public static Reply withImage(String url) {return new Reply(null, url, null, Type.IMAGE);}

    public static Reply withLocation(String address, Double latitude, Double longitude) {
        return new Reply(address, null, new Location(latitude, longitude), Type.MAP);
    }
}
