package com.gitlab.jantolis.chatroom.endpoints.responses;

import com.fasterxml.jackson.annotation.JsonCreator;

import static java.util.Arrays.stream;

public enum Sender {

    CLIENT("CLIENT"),
    USER("USER");

    private final String name;

    Sender(String name) {
        this.name = name;
    }

    @JsonCreator
    public static Sender fromName(final String name) {
        return stream(Sender.values())
                .filter(sender -> sender.name.equals(name))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No Sender found for name " + name));
    }
}
