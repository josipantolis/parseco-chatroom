package com.gitlab.jantolis.chatroom.endpoints;

import com.gitlab.jantolis.chatroom.endpoints.responses.CollectionResponse;
import com.gitlab.jantolis.chatroom.endpoints.responses.Conversations;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

import java.time.Instant;

public interface ConversationsEndpoint {

    @GET("conversations")
    Observable<CollectionResponse<Conversations>> getConversationsFrom(
            @Query("lastActivityFrom") Mappable<Instant> from
    );

}
