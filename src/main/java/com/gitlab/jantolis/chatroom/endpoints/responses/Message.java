package com.gitlab.jantolis.chatroom.endpoints.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gitlab.jantolis.chatroom.endpoints.Type;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.time.Instant;
import java.util.Optional;

@Getter
@ToString
@EqualsAndHashCode(of = "id")
public class Message {

    private final String id;
    private final String conversationId;
    private final Sender sender;
    private final Type type;
    private final Optional<String> text;
    private final Optional<String> url;
    private final Optional<Location> location;
    private final Instant sentAt;
    private final Boolean read;

    @JsonCreator
    public Message(
            @JsonProperty("id") String id,
            @JsonProperty("conversationId") String conversationId,
            @JsonProperty("sender") Sender sender,
            @JsonProperty("type") Type type,
            @JsonProperty("text") Optional<String> text,
            @JsonProperty("url") Optional<String> url,
            @JsonProperty("location") Optional<Location> location,
            @JsonProperty("sentAt") Instant sentAt,
            @JsonProperty("read") Boolean read) {
        this.id = id;
        this.conversationId = conversationId;
        this.sender = sender;
        this.type = type;
        this.text = text;
        this.url = url;
        this.location = location
                .filter(loc -> loc.getLatitude() != null)
                .filter(loc -> loc.getLongitude() != null);
        this.sentAt = sentAt;
        this.read = read;
    }
}
