package com.gitlab.jantolis.chatroom.endpoints.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Optional;

@Getter
@ToString
@EqualsAndHashCode(of = "id")
public class Conversation {

    private final String id;
    private final String name;
    private final Status status;
    private final Subscriber subscriber;
    private final Message previewMessage;

    @JsonCreator
    public Conversation(
            @JsonProperty("id") String id,
            @JsonProperty("name") String name,
            @JsonProperty("status") Status status,
            @JsonProperty("_embedded") ConversationDependants embedded) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.subscriber = Optional.ofNullable(embedded)
                .map(ConversationDependants::getSubscriber)
                .orElse(null);
        this.previewMessage = Optional.ofNullable(embedded)
                .map(ConversationDependants::getPreviewMessage)
                .orElse(null);
    }

}
