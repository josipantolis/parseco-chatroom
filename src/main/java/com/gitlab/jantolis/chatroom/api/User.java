package com.gitlab.jantolis.chatroom.api;

import java.util.Optional;

public interface User {

    String getId();

    String getConversationId();

    String getScreenName();

    void setScreenName(String newScreenName);

    Optional<String> getLastReceivedMessageId();

}
