package com.gitlab.jantolis.chatroom.api.message.content;

import com.gitlab.jantolis.chatroom.api.message.ContentMapper;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;

public class GeoContent implements MessageContent {

    private final Double latitude;
    private final Double longitude;
    private final String address;

    public GeoContent(Double latitude, Double longitude, String address) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public <T> T mapWith(ContentMapper<T> mapper) {
        return mapper.map(this);
    }

    @Override
    public String toString() {
        return "GeoContent{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", address='" + address + '\'' +
                '}';
    }
}
