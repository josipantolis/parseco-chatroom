package com.gitlab.jantolis.chatroom.api;

import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import org.springframework.core.Ordered;

public interface ResponseInterceptor extends Ordered {

    boolean isApplicableTo(ChatMessage originalMessage);

    void intercept(ResponseContext context);

}
