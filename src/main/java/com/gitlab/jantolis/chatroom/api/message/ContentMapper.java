package com.gitlab.jantolis.chatroom.api.message;

import com.gitlab.jantolis.chatroom.api.message.content.GeoContent;
import com.gitlab.jantolis.chatroom.api.message.content.ImageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;

public interface ContentMapper<T> {

    T map(TextContent textContent);

    T map(GeoContent geoContent);

    T map(ImageContent imageContent);
}
