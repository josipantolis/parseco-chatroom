package com.gitlab.jantolis.chatroom.api.message.mapper;

import com.gitlab.jantolis.chatroom.api.message.ContentMapper;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import com.gitlab.jantolis.chatroom.api.message.content.GeoContent;
import com.gitlab.jantolis.chatroom.api.message.content.ImageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

public final class ContentMappers {

    private ContentMappers() {
    }

    public static ContentMapper<MessageContent> textMapper(Function<TextContent, MessageContent> mapper) {
        return new BaseContentMapper() {
            @Override
            public MessageContent map(TextContent textContent) {
                return mapper.apply(textContent);
            }
        };
    }

    public static ContentMapper<MessageContent> geoMapper(Function<GeoContent, MessageContent> mapper) {
        return new BaseContentMapper() {
            @Override
            public MessageContent map(GeoContent geoContent) {
                return mapper.apply(geoContent);
            }
        };
    }

    public static ContentMapper<MessageContent> imageMapper(Function<ImageContent, MessageContent> mapper) {
        return new BaseContentMapper() {
            @Override
            public MessageContent map(ImageContent imageContent) {
                return mapper.apply(imageContent);
            }
        };
    }

    public static ContentMapper<Boolean> textPredicate(Predicate<TextContent> predicate) {
        return new BaseContentPredicate() {
            @Override
            public Boolean map(TextContent textContent) {
                return predicate.test(textContent);
            }
        };
    }

    public static ContentMapper<Boolean> geoPredicate(Predicate<GeoContent> predicate) {
        return new BaseContentPredicate() {
            @Override
            public Boolean map(GeoContent geoContent) {
                return predicate.test(geoContent);
            }
        };
    }

    public static ContentMapper<Boolean> imagePredicate(Predicate<ImageContent> predicate) {
        return new BaseContentPredicate() {
            @Override
            public Boolean map(ImageContent imageContent) {
                return predicate.test(imageContent);
            }
        };
    }

    public static <T> ContentMapper<Optional<T>> textExtractor(Function<TextContent, T> extractor) {
        return new BaseContentExtractor<T>() {
            @Override
            public Optional<T> map(TextContent textContent) {
                return Optional.of(textContent).map(extractor);
            }
        };
    }

    public static <T> ContentMapper<Optional<T>> geoExtractor(Function<GeoContent, T> extractor) {
        return new BaseContentExtractor<T>() {
            @Override
            public Optional<T> map(GeoContent geoContent) {
                return Optional.of(geoContent).map(extractor);
            }
        };
    }

    public static <T> ContentMapper<Optional<T>> imageExtractor(Function<ImageContent, T> extractor) {
        return new BaseContentExtractor<T>() {
            @Override
            public Optional<T> map(ImageContent imageContent) {
                return Optional.of(imageContent).map(extractor);
            }
        };
    }
}
