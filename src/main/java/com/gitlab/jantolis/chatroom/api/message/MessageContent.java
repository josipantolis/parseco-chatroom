package com.gitlab.jantolis.chatroom.api.message;

public interface MessageContent {

    <T> T mapWith(ContentMapper<T> mapper);

}
