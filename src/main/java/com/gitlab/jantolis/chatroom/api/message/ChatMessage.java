package com.gitlab.jantolis.chatroom.api.message;

import com.gitlab.jantolis.chatroom.api.User;

import java.time.Instant;

public interface ChatMessage {

    String id();

    MessageContent getContent();

    User getSender();

    Instant getSentAt();

}
