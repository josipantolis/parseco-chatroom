package com.gitlab.jantolis.chatroom.api;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public interface ChatRoom {

    class Key<T> {
        public final String name;
        public final Class<T> type;

        Key(String name, Class<T> type) {
            this.name = name;
            this.type = type;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Key<?> key = (Key<?>) o;
            return Objects.equals(name, key.name) &&
                    Objects.equals(type, key.type);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, type);
        }
    }

    static <T> Key<T> key(String name, Class<T> type) {
        return new Key<>(name, type);
    }

    Set<User> getAllUsers();

    <T> T put(Key<T> key, T value);

    <T> Optional<T> get(Key<T> key);

}
