package com.gitlab.jantolis.chatroom.api.message.mapper;

import com.gitlab.jantolis.chatroom.api.message.ContentMapper;
import com.gitlab.jantolis.chatroom.api.message.content.GeoContent;
import com.gitlab.jantolis.chatroom.api.message.content.ImageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;

import java.util.Optional;

public abstract class BaseContentExtractor<T> implements ContentMapper<Optional<T>> {

    @Override
    public Optional<T> map(TextContent textContent) {
        return Optional.empty();
    }

    @Override
    public Optional<T> map(GeoContent geoContent) {
        return Optional.empty();
    }

    @Override
    public Optional<T> map(ImageContent imageContent) {
        return Optional.empty();
    }
}
