package com.gitlab.jantolis.chatroom.api.message.mapper;

import com.gitlab.jantolis.chatroom.api.message.ContentMapper;
import com.gitlab.jantolis.chatroom.api.message.content.GeoContent;
import com.gitlab.jantolis.chatroom.api.message.content.ImageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;

public abstract class BaseContentPredicate implements ContentMapper<Boolean> {

    @Override
    public Boolean map(TextContent textContent) {
        return false;
    }

    @Override
    public Boolean map(GeoContent geoContent) {
        return false;
    }

    @Override
    public Boolean map(ImageContent imageContent) {
        return false;
    }
}
