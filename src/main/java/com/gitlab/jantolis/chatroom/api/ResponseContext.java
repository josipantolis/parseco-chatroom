package com.gitlab.jantolis.chatroom.api;

import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import rx.Observable;

import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public interface ResponseContext {

    ChatMessage getOriginalMessage();

    void addContentProcessor(UnaryOperator<Observable<MessageContent>> processor);

    void addRecipientFilter(Predicate<User> filter);

    void next();

}
