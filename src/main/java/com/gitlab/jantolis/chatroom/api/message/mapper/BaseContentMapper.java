package com.gitlab.jantolis.chatroom.api.message.mapper;

import com.gitlab.jantolis.chatroom.api.message.ContentMapper;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import com.gitlab.jantolis.chatroom.api.message.content.GeoContent;
import com.gitlab.jantolis.chatroom.api.message.content.ImageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;

public abstract class BaseContentMapper implements ContentMapper<MessageContent> {

    @Override
    public MessageContent map(TextContent textContent) {
        return textContent;
    }

    @Override
    public MessageContent map(GeoContent geoContent) {
        return geoContent;
    }

    @Override
    public MessageContent map(ImageContent imageContent) {
        return imageContent;
    }
}
