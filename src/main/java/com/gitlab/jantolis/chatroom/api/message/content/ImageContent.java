package com.gitlab.jantolis.chatroom.api.message.content;

import com.gitlab.jantolis.chatroom.api.message.ContentMapper;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;

public class ImageContent implements MessageContent {

    private final String imageUrl;

    public ImageContent(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public <T> T mapWith(ContentMapper<T> mapper) {
        return mapper.map(this);
    }

    @Override
    public String toString() {
        return "ImageContent{" +
                "imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
