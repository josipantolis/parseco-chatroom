package com.gitlab.jantolis.chatroom.api.message.content;

import com.gitlab.jantolis.chatroom.api.message.ContentMapper;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;

public class TextContent implements MessageContent {

    public final String text;

    public TextContent(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @Override
    public <T> T mapWith(ContentMapper<T> mapper) {
        return mapper.map(this);
    }

    @Override
    public String toString() {
        return "TextContent{" +
                "text='" + text + '\'' +
                '}';
    }
}
