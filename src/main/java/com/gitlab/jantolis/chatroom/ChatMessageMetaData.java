package com.gitlab.jantolis.chatroom;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.Instant;

@ToString
@EqualsAndHashCode(of = "id")
public class ChatMessageMetaData {
    protected final String id;
    protected final Instant sentAt;

    public ChatMessageMetaData(String id, Instant sentAt) {
        this.sentAt = sentAt;
        this.id = id;
    }

    public String id() {
        return id;
    }

    public Instant getSentAt() {
        return sentAt;
    }
}
