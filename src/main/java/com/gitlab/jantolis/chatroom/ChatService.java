package com.gitlab.jantolis.chatroom;

import com.gitlab.jantolis.chatroom.receiver.ConversationsService;
import com.gitlab.jantolis.chatroom.receiver.MessagesService;
import com.gitlab.jantolis.chatroom.sender.SenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import rx.Observable;

import java.time.Clock;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

@Service
public class ChatService implements CommandLineRunner {

    private final ConversationsService conversationsService;
    private final MessagesService messagesService;
    private final SenderService senderService;
    private final RefreshConfiguration config;
    private final Clock clock;

    @Autowired
    public ChatService(
            ConversationsService conversationsService,
            MessagesService messagesService,
            SenderService senderService,
            RefreshConfiguration refreshConfiguration,
            Clock clock) {
        this.conversationsService = conversationsService;
        this.messagesService = messagesService;
        this.senderService = senderService;
        this.config = refreshConfiguration;
        this.clock = clock;
    }

    @Override
    public void run(String... strings) throws Exception {
        Instant startedAt = Instant.now(clock);

        Observable.interval(config.getRate(), TimeUnit.MILLISECONDS).timeInterval()
                .map(interval -> interval.getIntervalInMilliseconds() * interval.getValue())
                .map(timePassed -> startedAt.plus(timePassed, ChronoUnit.MILLIS))
                .flatMap(conversationsService::fetchNewerThan)
                .startWith(conversationsService.fetchAll())
                .flatMap(messagesService::fetchUnreadFor)
                .flatMap(senderService::sendMessage)
                .toBlocking()
                .subscribe(n -> {
                }, Throwable::printStackTrace, () -> System.out.println("All done!"));
    }
}
