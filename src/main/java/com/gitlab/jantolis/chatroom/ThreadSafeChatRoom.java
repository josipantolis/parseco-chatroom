package com.gitlab.jantolis.chatroom;

import com.gitlab.jantolis.chatroom.api.ChatRoom;
import com.gitlab.jantolis.chatroom.api.User;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ThreadSafeChatRoom implements ChatRoom {

    private final Map<String, BasicUser> users = Collections.synchronizedMap(new HashMap<>());

    private final Map<ChatRoom.Key, Object> store = Collections.synchronizedMap(new HashMap<>());

    @Override
    public Set<User> getAllUsers() {
        synchronized (users) {
            return new HashSet<>(users.values());
        }
    }

    public void registerUser(BasicUser newUser) {
        synchronized (users) {
            users.put(newUser.getId(), newUser);
        }
    }

    public void registerMessageFor(ChatMessageMetaData message, String userId) {
        synchronized (users) {
            users.computeIfPresent(
                    userId,
                    (id, basicUser) -> basicUser.registerMessageReceipt(message)
            );
        }
    }

    @Override
    public <T> T put(Key<T> key, T value) {
        synchronized (store) {
            return key.type.cast(store.put(key, value));
        }
    }

    @Override
    public <T> Optional<T> get(Key<T> key) {
        synchronized (store) {
            return Optional.ofNullable(store.get(key))
                    .map(key.type::cast);
        }
    }
}
