package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import rx.Observable;

@Component
public class JonnyInterceptor extends CommandInterceptor {
    private static final String JONNY_COMMAND = "/jonny";

    @Override
    public String getCommand() {
        return JONNY_COMMAND;
    }

    @Override
    public String getCommandDescription() {
        return "Send Jonny's regards to everyone.";
    }

    @Override
    public void intercept(ResponseContext context) {
        context.addContentProcessor(messageText -> Observable.just(new TextContent("Jonny sends his regards!")));
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
