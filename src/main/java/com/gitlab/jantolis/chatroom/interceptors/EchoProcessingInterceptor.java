package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import rx.functions.Func1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.gitlab.jantolis.chatroom.api.message.mapper.ContentMappers.textMapper;

@Component
public class EchoProcessingInterceptor extends CommandInterceptor {

    public static final String ECHO_COMMAND = "^/echo (.*)";

    @Override
    public String getCommand() {
        return ECHO_COMMAND;
    }

    @Override
    public String getCommandDescription() {
        return "Ill-advised way of talking to yourself.";
    }

    @Override
    public void intercept(ResponseContext context) {
        context.addContentProcessor(contentObservable -> contentObservable.map(removeCommandPrefix()));
        context.next();
    }

    private Func1<MessageContent, MessageContent> removeCommandPrefix() {
        return content -> content.mapWith(textMapper(
                textContent -> {
                    Pattern commandPattern = Pattern.compile(ECHO_COMMAND);
                    Matcher matcher = commandPattern.matcher(textContent.getText());
                    if (matcher.matches()) {
                        return new TextContent(matcher.group(1));
                    } else {
                        return textContent;
                    }
                })
        );
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
