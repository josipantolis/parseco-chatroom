package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.User;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import rx.Observable;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class HelpInterceptor extends CommandInterceptor {

    private static final String HELP_COMMAND = "/help";

    @Lazy
    @Autowired
    private List<CommandInterceptor> allCommandInterceptors;

    @Override
    public String getCommand() {
        return HELP_COMMAND;
    }

    @Override
    public String getCommandDescription() {
        return "Get a list of all commands in a message visible only to you.";
    }

    @Override
    public void intercept(ResponseContext context) {
        User sender = context.getOriginalMessage().getSender();
        context.addRecipientFilter(sender::equals);
        context.addContentProcessor(ignorable -> Observable.just(
                new TextContent(allCommandInterceptors.stream()
                        .map(this::formatCommandDescription)
                        .collect(Collectors.joining("\n")))
        ));
    }

    private String formatCommandDescription(CommandInterceptor cmd) {
        return String.format("%s: %s", cmd.getCommand(), cmd.getCommandDescription());
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
