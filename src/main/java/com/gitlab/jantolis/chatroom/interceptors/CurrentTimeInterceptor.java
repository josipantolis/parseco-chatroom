package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.User;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import rx.Observable;

import java.util.Date;

@Component
public class CurrentTimeInterceptor extends CommandInterceptor {

    private static final String NOW_COMMAND = "/now";

    @Override
    public String getCommand() {
        return NOW_COMMAND;
    }

    @Override
    public String getCommandDescription() {
        return "Get current time in a message visible only to you.";
    }

    @Override
    public void intercept(ResponseContext context) {
        User sender = context.getOriginalMessage().getSender();
        context.addRecipientFilter(sender::equals);
        context.addContentProcessor(ignorable -> Observable.just(new TextContent(new Date().toString())));
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
