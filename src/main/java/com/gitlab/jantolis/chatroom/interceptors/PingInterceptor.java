package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.User;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import rx.Observable;

@Component
public class PingInterceptor extends CommandInterceptor {

    private static final String PING_COMMAND = "/ping";
    public static final String PONG_RESPONSE = "pong";

    @Override
    public String getCommand() {
        return PING_COMMAND;
    }

    @Override
    public String getCommandDescription() {
        return PONG_RESPONSE;
    }

    @Override
    public void intercept(ResponseContext context) {
        User sender = context.getOriginalMessage().getSender();
        context.addRecipientFilter(sender::equals);
        context.addContentProcessor(ignorable -> Observable.just(new TextContent(PONG_RESPONSE)));
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
