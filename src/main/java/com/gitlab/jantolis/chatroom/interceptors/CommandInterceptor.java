package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import com.gitlab.jantolis.chatroom.api.ResponseInterceptor;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static com.gitlab.jantolis.chatroom.api.message.mapper.ContentMappers.textPredicate;

public abstract class CommandInterceptor implements ResponseInterceptor {

    public abstract String getCommand();

    public abstract String getCommandDescription();

    @Override
    public boolean isApplicableTo(ChatMessage originalMessage) {
        String command = getCommand();
        return isApplicableTo(originalMessage, command);
    }

    public static boolean isApplicableTo(ChatMessage originalMessage, String command) {
        return originalMessage.getContent().mapWith(textPredicate(
                textContent -> {
                    if (isRegex(command)) {
                        return textContent.getText().matches(command);
                    } else {
                        return textContent.getText().startsWith(command);
                    }
                }
        ));
    }

    private static boolean isRegex(String s) {
        try {
            Pattern.compile(s);
            return true;
        } catch (PatternSyntaxException e) {
            return false;
        } catch (RuntimeException e) {
            return false;
        }
    }
}
