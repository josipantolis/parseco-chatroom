package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.ResponseInterceptor;
import com.gitlab.jantolis.chatroom.api.User;
import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import org.springframework.stereotype.Component;
import rx.Observable;

import java.util.function.UnaryOperator;

@Component
public class BroadcastMessageInterceptor implements ResponseInterceptor {

    @Override
    public boolean isApplicableTo(ChatMessage originalMessage) {
        return true;
    }

    @Override
    public void intercept(ResponseContext context) {
        User sender = context.getOriginalMessage().getSender();
        context.addRecipientFilter(user -> !sender.equals(user));
        context.addContentProcessor(prefixWithNameOf(sender));
        context.next();
    }

    private UnaryOperator<Observable<MessageContent>> prefixWithNameOf(User sender) {
        return observable -> observable.startWith(new TextContent(sender.getScreenName() + ":"));
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
