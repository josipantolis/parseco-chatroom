package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.ResponseInterceptor;
import com.gitlab.jantolis.chatroom.api.User;
import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import org.springframework.stereotype.Component;
import rx.Observable;

import java.util.function.UnaryOperator;

@Component
public class DirectMessageProcessingInterceptor implements ResponseInterceptor {
    @Override
    public boolean isApplicableTo(ChatMessage originalMessage) {
        boolean isDirectMessage = CommandInterceptor.isApplicableTo(
                originalMessage,
                DirectMessageRecipientFilterInterceptor.DIRECT_MESSAGE_COMMAND
        );
        boolean isDirectReply = CommandInterceptor.isApplicableTo(
                originalMessage,
                DirectReplyRecipientFilterInterceptor.DIRECT_REPLY_COMMAND
        );
        return isDirectMessage || isDirectReply;
    }

    @Override
    public void intercept(ResponseContext context) {
        User sender = context.getOriginalMessage().getSender();
        context.addContentProcessor(indicatePrivateMessage(sender));
        context.next();
    }

    private UnaryOperator<Observable<MessageContent>> indicatePrivateMessage(User sender) {
        return observable -> observable
                .skip(1)
                .startWith(new TextContent(sender.getScreenName() + " privately says:"));
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
