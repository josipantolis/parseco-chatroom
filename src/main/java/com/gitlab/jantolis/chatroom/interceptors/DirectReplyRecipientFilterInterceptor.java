package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.api.ChatRoom;
import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.User;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import rx.Observable;

import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.gitlab.jantolis.chatroom.api.ChatRoom.key;
import static com.gitlab.jantolis.chatroom.api.message.mapper.ContentMappers.textMapper;
import static com.gitlab.jantolis.chatroom.interceptors.DirectMessageRecipientFilterInterceptor.KEY_PREFIX;

@Component
public class DirectReplyRecipientFilterInterceptor extends CommandInterceptor {

    public final static String DIRECT_REPLY_COMMAND = "^/reply:(.+)";
    private static final Pattern DIRECT_REPLY_PATTERN = Pattern.compile(DIRECT_REPLY_COMMAND);
    private static final String NO_PREVIOUS_DIRECT_MESSAGE_FOUND =
            "Can't find previous private message to reply to.\n" +
                    "Try sending a private message by targeting user by name, " +
                    "for example: @John: How's life on Mars?\n" +
                    "For additional help send /help.";

    private final ChatRoom chatRoom;

    @Autowired
    public DirectReplyRecipientFilterInterceptor(ChatRoom chatRoom) {
        this.chatRoom = chatRoom;
    }

    @Override
    public String getCommand() {
        return DIRECT_REPLY_COMMAND;
    }

    @Override
    public String getCommandDescription() {
        return "Privately reply to the last person who sent you a private message.";
    }

    @Override
    public void intercept(ResponseContext context) {
        User sender = context.getOriginalMessage().getSender();
        chatRoom.get(key(KEY_PREFIX + sender.getId(), User.class))
                .map(recipient -> {
                    chatRoom.put(key(KEY_PREFIX + recipient.getId(), User.class), sender);
                    context.addRecipientFilter(recipient::equals);
                    context.addContentProcessor(getContentMapper());
                    context.next();
                    return recipient;
                }).orElseGet(() -> {
            context.addRecipientFilter(sender::equals);
            context.addContentProcessor(ignorable ->
                    Observable.just(new TextContent(NO_PREVIOUS_DIRECT_MESSAGE_FOUND)));
            return null;
        });
    }


    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    private UnaryOperator<Observable<MessageContent>> getContentMapper() {
        return observableContent ->
                observableContent.map(content ->
                        content.mapWith(textMapper(textContent -> {
                            Matcher matcher = DIRECT_REPLY_PATTERN.matcher(textContent.getText());
                            if (matcher.matches()) {
                                return new TextContent(matcher.group(1));
                            }
                            return textContent;
                        }))
                );
    }
}
