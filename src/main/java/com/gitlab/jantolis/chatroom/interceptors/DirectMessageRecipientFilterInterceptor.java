package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.api.ChatRoom;
import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.User;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import rx.Observable;

import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.gitlab.jantolis.chatroom.api.ChatRoom.key;
import static com.gitlab.jantolis.chatroom.api.message.mapper.ContentMappers.textExtractor;
import static com.gitlab.jantolis.chatroom.api.message.mapper.ContentMappers.textMapper;

@Component
public class DirectMessageRecipientFilterInterceptor extends CommandInterceptor {

    public final static String DIRECT_MESSAGE_COMMAND = "^@(.+):(.+)";
    public static final Pattern DIRECT_MESSAGE_PATTERN = Pattern.compile(DIRECT_MESSAGE_COMMAND);
    public static final String KEY_PREFIX = DirectMessageRecipientFilterInterceptor.class.getPackage().getName() + ":";
    private static final String USER_NOT_FOUND = "Can't find a user named %s.\n" +
            "Try looking them up with /who command.\n" +
            "For additional help send /help.";

    private final ChatRoom chatRoom;

    @Autowired
    public DirectMessageRecipientFilterInterceptor(ChatRoom chatRoom) {
        this.chatRoom = chatRoom;
    }


    @Override
    public String getCommand() {
        return DIRECT_MESSAGE_COMMAND;
    }

    @Override
    public String getCommandDescription() {
        return "Send a private message to another user via their name.";
    }

    @Override
    public void intercept(ResponseContext context) {
        String originalText = context.getOriginalMessage()
                .getContent()
                .mapWith(textExtractor(TextContent::getText))
                .get();
        User sender = context.getOriginalMessage().getSender();
        Matcher matcher = DIRECT_MESSAGE_PATTERN.matcher(originalText);
        matcher.matches();
        String targetedUserName = matcher.group(1);
        long recipientsCount = chatRoom.getAllUsers().stream()
                .filter(user -> user.getScreenName().equals(targetedUserName))
                .map(recipient -> {
                    chatRoom.put(key(KEY_PREFIX + recipient.getId(), User.class), sender);
                    context.addRecipientFilter(recipient::equals);
                    return recipient;
                }).count();

        if (recipientsCount > 0) {
            context.addContentProcessor(getContentMapper());
            context.next();
            return;
        }

        context.addRecipientFilter(sender::equals);
        context.addContentProcessor(ignorable ->
                Observable.just(new TextContent(String.format(USER_NOT_FOUND, targetedUserName))));
    }

    public UnaryOperator<Observable<MessageContent>> getContentMapper() {
        return observableContent ->
                observableContent.map(content ->
                        content.mapWith(textMapper(textContent -> {
                            Matcher matcher = DIRECT_MESSAGE_PATTERN.matcher(textContent.getText());
                            if (matcher.matches()) {
                                return new TextContent(matcher.group(2));
                            }
                            return textContent;
                        }))
                );
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
