package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.api.ChatRoom;
import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.User;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import rx.Observable;

import java.util.stream.Collectors;

@Component
public class ListSubscribersInterceptor extends CommandInterceptor {

    private static final String WHO_COMMAND = "/who";

    private final ChatRoom chatRoomContext;

    @Autowired
    public ListSubscribersInterceptor(ChatRoom chatRoomContext) {
        this.chatRoomContext = chatRoomContext;
    }

    @Override
    public String getCommand() {
        return WHO_COMMAND;
    }

    @Override
    public String getCommandDescription() {
        return "Get a list of all chat participants in a message visible only to you.";
    }

    @Override
    public void intercept(ResponseContext context) {
        User sender = context.getOriginalMessage().getSender();
        context.addRecipientFilter(sender::equals);
        context.addContentProcessor(ignorable -> Observable.just(
                new TextContent(
                        "Users currently in this chat:\n" +
                                chatRoomContext.getAllUsers().stream()
                                        .map(User::getScreenName)
                                        .collect(Collectors.joining("\n"))
                )
        ));
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
