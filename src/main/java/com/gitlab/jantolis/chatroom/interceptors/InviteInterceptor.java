package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.ParsecoConfiguration;
import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import com.gitlab.jantolis.chatroom.endpoints.InviteEndpoint;
import com.gitlab.jantolis.chatroom.endpoints.requests.Invitation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import rx.Observable;

import static com.gitlab.jantolis.chatroom.api.message.mapper.ContentMappers.textExtractor;

@Component
public class InviteInterceptor extends CommandInterceptor {

    private static final String INVITE_COMMAND = "^/invite [0-9\\-\\+]{9,15}$";

    private final InviteEndpoint inviteEndpoint;
    private final ParsecoConfiguration config;

    @Autowired
    public InviteInterceptor(
            InviteEndpoint inviteEndpoint,
            ParsecoConfiguration parsecoConfiguration) {
        this.inviteEndpoint = inviteEndpoint;
        this.config = parsecoConfiguration;
    }

    @Override
    public String getCommand() {
        return INVITE_COMMAND;
    }

    @Override
    public String getCommandDescription() {
        return "Invite anyone to this chat room.";
    }

    @Override
    public void intercept(ResponseContext context) {
        String senderName = context.getOriginalMessage().getSender().getScreenName();
        context.getOriginalMessage()
                .getContent()
                .mapWith(textExtractor(TextContent::getText))
                .map(text -> text.replace("/invite ", ""))
                .ifPresent(phoneNumber -> context
                        .addContentProcessor(ignorable -> invite(senderName, phoneNumber)
                                .take(1)
                                .map(v -> new TextContent(senderName + " just invited " + phoneNumber))
                        )
                );
    }

    private Observable<Void> invite(String senderName, String phoneNumber) {
        String message = String.format("Hi there! " +
                        "%s would like to chat with you in %s on Parseco!" +
                        " Join in by downloading app and following in-app instructions.",
                senderName, config.getChannel().getTitle());
        return inviteEndpoint.sendInvitation(config.getChannel().getId(), new Invitation(phoneNumber, message))
                .onExceptionResumeNext(Observable.<Void>empty());
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
