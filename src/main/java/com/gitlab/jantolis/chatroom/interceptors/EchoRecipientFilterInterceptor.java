package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.ResponseInterceptor;
import com.gitlab.jantolis.chatroom.api.User;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import static com.gitlab.jantolis.chatroom.api.message.mapper.ContentMappers.textPredicate;

@Component
public class EchoRecipientFilterInterceptor implements ResponseInterceptor {

    @Override
    public boolean isApplicableTo(ChatMessage originalMessage) {
        return CommandInterceptor.isApplicableTo(originalMessage, EchoProcessingInterceptor.ECHO_COMMAND);
    }

    @Override
    public void intercept(ResponseContext context) {
        User sender = context.getOriginalMessage().getSender();
        context.addRecipientFilter(sender::equals);
    }

    @Override
    public int getOrder() {
        return -1;
    }
}
