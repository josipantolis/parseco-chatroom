package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.Pair;
import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.ResponseInterceptor;
import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Service;
import rx.Observable;

import java.util.concurrent.atomic.AtomicInteger;

import static com.gitlab.jantolis.chatroom.api.message.mapper.ContentMappers.textPredicate;

@Service
public class TextContentGroupingInterceptor implements ResponseInterceptor{
    @Override
    public boolean isApplicableTo(ChatMessage originalMessage) {
        return true;
    }

    @Override
    public void intercept(ResponseContext context) {
        context.addContentProcessor(this::groupTextContentTogether);
        context.next();
    }

    public Observable<MessageContent> groupTextContentTogether(Observable<MessageContent> source) {
        AtomicInteger counter = new AtomicInteger(0);

        return source.map(content -> new Pair<>(counter.getAndIncrement(), content))
                .scan((pair1, pair2) -> {
                    if (bothAreTextContent(pair1.getValue(), pair2.getValue())) {
                        return new Pair<>(
                                pair1.getKey(),
                                pair1.getValue().mapWith(new TextAppender(pair2.getValue())));
                    }
                    return pair2;
                }).groupBy(Pair::getKey)
                .flatMap(Observable::last)
                .map(Pair::getValue);
    }

    private Boolean bothAreTextContent(MessageContent content1, MessageContent content2) {
        return content1.mapWith(textPredicate(txt -> true)) && content2.mapWith(textPredicate(txt -> true));
    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }
}
