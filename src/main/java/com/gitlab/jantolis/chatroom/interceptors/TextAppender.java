package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import com.gitlab.jantolis.chatroom.api.message.mapper.BaseContentMapper;

import static com.gitlab.jantolis.chatroom.api.message.mapper.ContentMappers.textExtractor;

public class TextAppender extends BaseContentMapper {

    private final String textToAppend;

    public TextAppender(MessageContent appendix) {
        textToAppend = appendix.mapWith(textExtractor(TextContent::getText))
                .map(text -> "\n" + text)
                .orElse("");
    }

    @Override
    public MessageContent map(TextContent textContent) {
        return new TextContent(textContent.getText() + textToAppend);
    }
}
