package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.ResponseInterceptor;
import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import org.springframework.stereotype.Component;
import rx.functions.Func1;

import java.util.HashMap;
import java.util.Map;

import static com.gitlab.jantolis.chatroom.api.message.mapper.ContentMappers.textMapper;

@Component
public class EmoticonInterceptor implements ResponseInterceptor {


    private final Map<String, String> regexToUnicodeCharacter;

    public EmoticonInterceptor() {
        regexToUnicodeCharacter = new HashMap<>(2);
        regexToUnicodeCharacter.put(":[-=]?[\\)D\\]}]+", "\uD83D\uDE03");
        regexToUnicodeCharacter.put(":[-=]?[\\(\\[{C]+", "\uD83D\uDE22");
    }

    @Override
    public boolean isApplicableTo(ChatMessage originalMessage) {
        return true;
    }

    @Override
    public void intercept(ResponseContext context) {
        context.addContentProcessor(contentObservable -> contentObservable
                .map(replaceEmoticonsInText())
        );

        context.next();
    }

    private Func1<MessageContent, MessageContent> replaceEmoticonsInText() {
        return content -> content.mapWith(textMapper(textContent -> {
            String emojifiedText = textContent.getText();
            for (Map.Entry<String, String> pair : regexToUnicodeCharacter.entrySet()) {
                emojifiedText = emojifiedText.replaceAll(pair.getKey(), pair.getValue());
            }
            return new TextContent(emojifiedText);
        }));
    }

    @Override
    public int getOrder() {
        return -10;
    }
}
