package com.gitlab.jantolis.chatroom;

import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import com.gitlab.jantolis.chatroom.api.User;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import lombok.ToString;

import java.time.Instant;

@ToString
public class BasicChatMessage extends ChatMessageMetaData implements ChatMessage {

    private final MessageContent content;
    protected final BasicUser sender;

    public BasicChatMessage(
            String id,
            MessageContent content,
            BasicUser sender,
            Instant sentAt) {
        super(id, sentAt);
        this.sender = sender;
        this.content = content;
    }

    @Override
    public MessageContent getContent() {
        return content;
    }

    @Override
    public User getSender() {
        return sender;
    }
}
