package com.gitlab.jantolis.chatroom;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@Configuration
@ConfigurationProperties(prefix = "parseco")
public class ParsecoConfiguration {

    @Getter
    @Setter
    @ToString
    public static class Channel {
        @NotBlank
        private String title;
        @NotBlank
        private String id;
    }

    @Getter
    @Setter
    @ToString
    public static class Api {
        @NotBlank
        private String base;
        @NotBlank
        private String authorization;
    }

    @NotNull
    private Channel channel;
    @NotNull
    private Api api;

}
