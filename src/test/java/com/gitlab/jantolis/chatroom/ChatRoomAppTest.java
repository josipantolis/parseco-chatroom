package com.gitlab.jantolis.chatroom;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

import static org.assertj.core.api.BDDAssertions.then;

public class ChatRoomAppTest {

    @Test
    public void shouldConfigureObjectMapperToMapDates() throws Exception {
        Date givenDate = Date.from(LocalDateTime.of(2015, 2, 19, 23, 59, 25).atZone(ZoneId.systemDefault()).toInstant());
        ObjectMapper givenMapper = new ChatRoomApp().objectMapper();

        String actualDateAsString = givenMapper.writeValueAsString(givenDate);

        then(actualDateAsString).isEqualTo("\"2015-02-19T23:59:25.000Z\"");

    }

    @Test
    public void shouldConfigureClockToUseUTC() throws Exception {
        Clock clock = new ChatRoomApp().clock();
        ZoneId actual = clock.getZone();
        then(actual).isEqualTo(ZoneOffset.UTC);
    }
}