package com.gitlab.jantolis.chatroom;

import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.ResponseInterceptor;
import com.gitlab.jantolis.chatroom.api.User;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import com.gitlab.jantolis.chatroom.sender.BasicResponseContext;
import org.assertj.core.api.BDDAssertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.Ordered;
import rx.Observable;

import java.util.List;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

import static com.gitlab.jantolis.chatroom.api.message.mapper.ContentMappers.textExtractor;
import static com.gitlab.jantolis.chatroom.api.message.mapper.ContentMappers.textMapper;
import static java.util.Arrays.asList;
import static java.util.function.UnaryOperator.identity;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;

@RunWith(MockitoJUnitRunner.class)
public class BasicResponseContextTest {

    public static final Predicate<User> ACCEPT_USER = user -> true;
    public static final Predicate<User> REJECT_USER = user -> false;

    @Mock
    private ChatMessage message;

    @Mock
    private User user;

    @Test
    public void shouldInvokeNextInterceptorOnNextCallback() throws Exception {
        //given
        ResponseInterceptor firstInterceptor = givenApplicableInterceptor();
        givenIntercepting(firstInterceptor, ACCEPT_USER, identity());
        ResponseInterceptor secondInterceptor = givenApplicableInterceptor();
        givenIntercepting(secondInterceptor, ACCEPT_USER, identity());
        List<ResponseInterceptor> interceptors = asList(firstInterceptor, secondInterceptor);

        //when
        BasicResponseContext context = new BasicResponseContext(message, interceptors);

        //then
        then(firstInterceptor).should().intercept(eq(context));
        then(secondInterceptor).should().intercept(eq(context));
    }

    @Test
    public void shouldInvokeInterceptorsInOrder() throws Exception {
        //given
        ResponseInterceptor higherOrderInterceptor = givenApplicableInterceptor();
        given(higherOrderInterceptor.getOrder()).willReturn(Ordered.HIGHEST_PRECEDENCE);
        givenIntercepting(higherOrderInterceptor, ACCEPT_USER, identity());
        ResponseInterceptor loverOrderInterceptor = givenApplicableInterceptor();
        given(loverOrderInterceptor.getOrder()).willReturn(Ordered.LOWEST_PRECEDENCE);
        givenIntercepting(loverOrderInterceptor, ACCEPT_USER, identity());
        InOrder inOrder = inOrder(higherOrderInterceptor, loverOrderInterceptor);
        List<ResponseInterceptor> interceptors = asList(loverOrderInterceptor, higherOrderInterceptor);

        //when
        new BasicResponseContext(message, interceptors);

        //then
        inOrder.verify(higherOrderInterceptor).intercept(any());
        inOrder.verify(loverOrderInterceptor).intercept(any());
    }


    @Test
    public void shouldStopInvokingWhenNextIsNotCalled() throws Exception {
        //given
        ResponseInterceptor finalInterceptor = givenApplicableInterceptor();
        given(finalInterceptor.getOrder()).willReturn(Ordered.HIGHEST_PRECEDENCE);
        givenFinalIntercepting(finalInterceptor, ACCEPT_USER, identity());
        ResponseInterceptor nextCallingInterceptor = givenApplicableInterceptor();
        given(nextCallingInterceptor.getOrder()).willReturn(Ordered.LOWEST_PRECEDENCE);
        givenIntercepting(nextCallingInterceptor, ACCEPT_USER, identity());
        List<ResponseInterceptor> interceptors = asList(finalInterceptor, nextCallingInterceptor);

        //when
        new BasicResponseContext(message, interceptors);

        //then
        then(finalInterceptor).should().intercept(any());
        then(nextCallingInterceptor).should(never()).intercept(any());
    }

    @Test
    public void shouldOnlyInvokeApplicableInterceptors() throws Exception {
        //given
        ResponseInterceptor firstApplicableInterceptor = givenApplicableInterceptor();
        givenIntercepting(firstApplicableInterceptor, ACCEPT_USER, identity());
        ResponseInterceptor inapplicableInterceptor = givenInapplicableInterceptor();
        givenIntercepting(inapplicableInterceptor, ACCEPT_USER, identity());
        ResponseInterceptor secondApplicableInterceptor = givenApplicableInterceptor();
        givenIntercepting(secondApplicableInterceptor, ACCEPT_USER, identity());
        List<ResponseInterceptor> interceptors = asList(
                firstApplicableInterceptor,
                inapplicableInterceptor,
                secondApplicableInterceptor);

        //when
        new BasicResponseContext(message, interceptors);

        //then
        then(firstApplicableInterceptor).should().intercept(any());
        then(inapplicableInterceptor).should(never()).intercept(any());
        then(secondApplicableInterceptor).should().intercept(any());
    }

    @Test
    public void shouldAcceptUserIfAllInterceptorsAccept() throws Exception {
        //given
        ResponseInterceptor firstInterceptor = givenApplicableInterceptor();
        givenIntercepting(firstInterceptor, ACCEPT_USER, identity());
        ResponseInterceptor secondInterceptor = givenApplicableInterceptor();
        givenIntercepting(secondInterceptor, ACCEPT_USER, identity());
        List<ResponseInterceptor> interceptors = asList(firstInterceptor, secondInterceptor);

        BasicResponseContext context = new BasicResponseContext(message, interceptors);

        //when
        boolean actual = context.shouldReceive(user);

        //then
        BDDAssertions.then(actual).isTrue();
    }

    @Test
    public void shouldRejectUserIfSingleInterceptorRejects() throws Exception {
        //given
        ResponseInterceptor acceptingInterceptor = givenApplicableInterceptor();
        givenIntercepting(acceptingInterceptor, ACCEPT_USER, identity());
        ResponseInterceptor rejectingInterceptor = givenApplicableInterceptor();
        givenIntercepting(rejectingInterceptor, REJECT_USER, identity());
        List<ResponseInterceptor> interceptors = asList(acceptingInterceptor, rejectingInterceptor);

        BasicResponseContext context = new BasicResponseContext(message, interceptors);

        //when
        boolean actual = context.shouldReceive(user);

        //then
        BDDAssertions.then(actual).isFalse();
    }

    @Test
    public void shouldComposeResponseProcessors() throws Exception {
        //given
        String startingText = "Starting text ";
        given(message.getContent()).willReturn(new TextContent(startingText));
        ResponseInterceptor firstInterceptor = givenApplicableInterceptor();
        String firstResponsePart = "1st part ";
        givenIntercepting(firstInterceptor, ACCEPT_USER, append(firstResponsePart));
        ResponseInterceptor secondInterceptor = givenApplicableInterceptor();
        String secondResponsePart = "2nd part ";
        givenIntercepting(secondInterceptor, ACCEPT_USER, append(secondResponsePart));
        List<ResponseInterceptor> interceptors = asList(firstInterceptor, secondInterceptor);

        BasicResponseContext context = new BasicResponseContext(message, interceptors);

        //when
        String actual = context.generateResponseContent()
                .toBlocking()
                .first()
                .mapWith(textExtractor(TextContent::getText)).get();

        //then
        BDDAssertions.then(actual).isEqualTo(startingText + firstResponsePart + secondResponsePart);
    }

    private UnaryOperator<Observable<MessageContent>> append(String appendix) {
        return response -> response.map(content -> content.mapWith(
                textMapper(text -> new TextContent(text.getText() + appendix))
        ));
    }


    private ResponseInterceptor givenApplicableInterceptor() {
        ResponseInterceptor mock = Mockito.mock(ResponseInterceptor.class);
        given(mock.isApplicableTo(any())).willReturn(true);
        return mock;
    }

    private ResponseInterceptor givenInapplicableInterceptor() {
        ResponseInterceptor mock = Mockito.mock(ResponseInterceptor.class);
        given(mock.isApplicableTo(any())).willReturn(false);
        return mock;
    }

    private void givenIntercepting(
            ResponseInterceptor interceptor,
            Predicate<User> subscriberFilter,
            UnaryOperator<Observable<MessageContent>> processor) {
        Mockito.doAnswer(invocation -> {
            ResponseContext context = (ResponseContext) invocation.getArguments()[0];
            context.addRecipientFilter(subscriberFilter);
            context.addContentProcessor(processor);
            context.next();
            return null;
        }).when(interceptor).intercept(any());
    }

    private void givenFinalIntercepting(
            ResponseInterceptor interceptor,
            Predicate<User> subscriberFilter,
            UnaryOperator<Observable<MessageContent>> processor) {
        Mockito.doAnswer(invocation -> {
            ResponseContext context = (ResponseContext) invocation.getArguments()[0];
            context.addRecipientFilter(subscriberFilter);
            context.addContentProcessor(processor);
            return null;
        }).when(interceptor).intercept(any());
    }
}
