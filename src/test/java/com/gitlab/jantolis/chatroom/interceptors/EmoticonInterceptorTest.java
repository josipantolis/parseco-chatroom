package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.Ordered;
import rx.Observable;

import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.gitlab.jantolis.chatroom.api.message.mapper.ContentMappers.textExtractor;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;

@RunWith(MockitoJUnitRunner.class)
public class EmoticonInterceptorTest {

    @Mock
    private ChatMessage originalMessage;

    @Mock
    private ResponseContext context;

    @Captor
    private ArgumentCaptor<UnaryOperator<Observable<MessageContent>>> contentProcessorCaptor;

    private EmoticonInterceptor interceptor = new EmoticonInterceptor();

    @Test
    public void shouldHaveMediumPrecedence() throws Exception {
        //when
        int actual = interceptor.getOrder();

        //then
        then(actual).isLessThan(Ordered.LOWEST_PRECEDENCE);
        then(actual).isGreaterThan(Ordered.HIGHEST_PRECEDENCE);
    }

    @Test
    public void shouldAlwaysBeApplicable() throws Exception {
        //when
        boolean actual = interceptor.isApplicableTo(null);

        //then
        then(actual).isTrue();
    }

    @Test
    public void shouldNotFilterRecipient() throws Exception {
        //when
        interceptor.intercept(context);

        //then
        BDDMockito.then(context).should(never()).addRecipientFilter(any());
    }

    @Test
    public void replaceSmileyFaceWithEmoticon() throws Exception {
        //given
        Observable<MessageContent> givenTextInChunks = Observable.just(
                "John Boone",
                "Went to the moon",
                "No fast cars :(",
                "He went to Mars :)!"
        ).map(TextContent::new);


        //when
        interceptor.intercept(context);

        //then
        BDDMockito.then(context).should().addContentProcessor(contentProcessorCaptor.capture());
        Iterable<String> messageComponents = () -> contentProcessorCaptor.getValue()
                .apply(givenTextInChunks)
                .map(content -> content.mapWith(textExtractor(TextContent::getText)).get())
                .toBlocking()
                .getIterator();
        String actual = StreamSupport.stream(
                messageComponents.spliterator(),
                false
        ).collect(Collectors.joining());
        String expected = "John Boone" +
                "Went to the moon" +
                "No fast cars \uD83D\uDE22" +
                "He went to Mars \uD83D\uDE03!";
        then(actual).isEqualTo(expected);
    }

    @Test
    public void shouldCallNextOnContext() throws Exception {
        //when
        interceptor.intercept(context);

        //then
        BDDMockito.then(context).should().next();
    }
}