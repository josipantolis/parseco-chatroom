package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.BasicUser;
import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.User;
import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rx.Observable;

import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.gitlab.jantolis.chatroom.api.message.mapper.ContentMappers.textExtractor;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class BroadcastMessageInterceptorTest {

    @Mock
    private ChatMessage originalMessage;

    @Mock
    private ResponseContext context;

    @Captor
    private ArgumentCaptor<Predicate<User>> recipientFilterCaptor;

    @Captor
    private ArgumentCaptor<UnaryOperator<Observable<MessageContent>>> contentProcessorCaptor;

    private BroadcastMessageInterceptor interceptor = new BroadcastMessageInterceptor();

    @Before
    public void setUp() throws Exception {
        given(context.getOriginalMessage()).willReturn(originalMessage);
    }

    @Test
    public void shouldHaveMiddlePrecedence() throws Exception {
        //when
        int actual = interceptor.getOrder();

        //then
        then(actual).isEqualTo(0);
    }

    @Test
    public void shouldAlwaysBeApplicable() throws Exception {
        //when
        boolean actual = interceptor.isApplicableTo(null);

        //then
        then(actual).isTrue();
    }

    @Test
    public void shouldPrefixResponseWithSendersName() throws Exception {
        //given
        String originalText = "Original text.";
        given(originalMessage.getContent()).willReturn(new TextContent(originalText));
        User sender = givenUserWithId("ID_1");
        given(originalMessage.getSender()).willReturn(sender);

        //when
        interceptor.intercept(context);

        //then
        BDDMockito.then(context).should().addContentProcessor(contentProcessorCaptor.capture());
        Iterable<String> messageComponents = () -> contentProcessorCaptor.getValue()
                .apply(Observable.just(new TextContent(originalText)))
                .map(content -> content.mapWith(textExtractor(TextContent::getText)).get())
                .toBlocking()
                .getIterator();
        String actual = StreamSupport.stream(
                messageComponents.spliterator(),
                false
        ).collect(Collectors.joining("\n"));
        String expectedText = String.format("%s:\n%s", sender.getScreenName(), originalText);
        then(actual).isEqualTo(expectedText);
    }

    @Test
    public void shouldSendResponseToEveryoneExceptOriginalMessageSender() throws Exception {
        //given
        User sender = givenUserWithId("ID_1");
        given(originalMessage.getSender()).willReturn(sender);
        User responseRecipient = givenUserWithId("ID_2");

        //when
        interceptor.intercept(context);

        //then
        BDDMockito.then(context).should().addRecipientFilter(recipientFilterCaptor.capture());
        Predicate<User> actual = recipientFilterCaptor.getValue();
        then(actual.test(sender)).isFalse();
        then(actual.test(responseRecipient)).isTrue();
    }

    private User givenUserWithId(String id) {
        return new BasicUser(id, "John", "Boone");
    }
}