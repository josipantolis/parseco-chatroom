package com.gitlab.jantolis.chatroom.interceptors;

import com.gitlab.jantolis.chatroom.BasicUser;
import com.gitlab.jantolis.chatroom.api.message.ChatMessage;
import com.gitlab.jantolis.chatroom.api.ResponseContext;
import com.gitlab.jantolis.chatroom.api.User;
import com.gitlab.jantolis.chatroom.api.message.MessageContent;
import com.gitlab.jantolis.chatroom.api.message.content.TextContent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.Ordered;
import rx.Observable;

import java.util.function.Predicate;
import java.util.function.UnaryOperator;

import static com.gitlab.jantolis.chatroom.api.message.mapper.ContentMappers.textExtractor;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;

@RunWith(MockitoJUnitRunner.class)
public class PingInterceptorTest {

    private static final String PING_COMMAND = "/ping";

    @Mock
    private ChatMessage originalMessage;

    @Mock
    private ResponseContext context;

    @Captor
    private ArgumentCaptor<Predicate<User>> recipientFilterCaptor;

    @Captor
    private ArgumentCaptor<UnaryOperator<Observable<MessageContent>>> contentProcessorCaptor;

    private PingInterceptor interceptor = new PingInterceptor();

    @Before
    public void setUp() throws Exception {
        given(context.getOriginalMessage()).willReturn(originalMessage);
    }

    @Test
    public void shouldHaveHighestPrecedence() throws Exception {
        //when
        int actual = interceptor.getOrder();

        //then
        then(actual).isEqualTo(Ordered.HIGHEST_PRECEDENCE);
    }

    @Test
    public void shouldBeApplicableToMessageStartingWithPingCommand() throws Exception {
        //given
        given(originalMessage.getContent()).willReturn(new TextContent(PING_COMMAND));

        //when
        boolean actual = interceptor.isApplicableTo((ChatMessage) originalMessage);

        //then
        then(actual).isTrue();
    }

    @Test
    public void shouldNotBeApplicableToMessageNotStartingWithPingCommand() throws Exception {
        //given
        given(originalMessage.getContent()).willReturn(new TextContent("Hello world!"));

        //when
        boolean actual = interceptor.isApplicableTo((ChatMessage) originalMessage);

        //then
        then(actual).isFalse();
    }

    @Test
    public void shouldSendResponseOnlyToOriginalMessageSender() throws Exception {
        //given
        given(originalMessage.getContent()).willReturn(new TextContent(PING_COMMAND));
        User sender = givenUserWithId("ID_1");
        given(originalMessage.getSender()).willReturn(sender);
        User otherUser = givenUserWithId("ID_2");

        //when
        interceptor.intercept(context);

        //then
        BDDMockito.then(context).should().addRecipientFilter(recipientFilterCaptor.capture());
        Predicate<User> actual = recipientFilterCaptor.getValue();
        then(actual.test(sender)).isTrue();
        then(actual.test(otherUser)).isFalse();
    }

    @Test
    public void shouldRespondWithPong() throws Exception {
        //given
        given(originalMessage.getContent()).willReturn(new TextContent(PING_COMMAND));
        given(originalMessage.getSender()).willReturn(givenUserWithId("ID_1"));

        //when
        interceptor.intercept(context);

        //then
        BDDMockito.then(context).should().addContentProcessor(contentProcessorCaptor.capture());
        MessageContent actual = contentProcessorCaptor.getValue()
                .apply(Observable.just(new TextContent("")))
                .toBlocking()
                .first();
        then(actual.mapWith(textExtractor(TextContent::getText)).get()).isEqualTo("pong");
    }

    @Test
    public void shouldNotInvokeNextOnContext() throws Exception {
        //given
        given(originalMessage.getContent()).willReturn(new TextContent(PING_COMMAND));
        given(originalMessage.getSender()).willReturn(givenUserWithId("ID_1"));

        //when
        interceptor.intercept(context);

        //then
        BDDMockito.then(context).should(never()).next();
    }

    private User givenUserWithId(String id) {
        return new BasicUser(id, "John", "Boone");
    }
}