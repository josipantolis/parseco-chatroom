package com.gitlab.jantolis.chatroom;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.jantolis.chatroom.api.ChatRoom;
import com.gitlab.jantolis.chatroom.endpoints.ConversationsEndpoint;
import com.gitlab.jantolis.chatroom.endpoints.responses.Conversation;
import com.gitlab.jantolis.chatroom.receiver.ConversationsService;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rx.Observable;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class ConversationsServiceTest extends TestCase {

	@Mock
	private ObjectMapper objectMapper;

	@Mock
	private ChatRoom chatRoomContext;

	@Mock
	private ConversationsEndpoint conversationsEndpoint;

	@InjectMocks
	private ConversationsService service;

	@Test
	public void testReturnEmptyObservableIeEndpointEmitsException() throws Exception {
		//given
		given(conversationsEndpoint.getConversationsFrom(any())).willReturn(Observable.error(new RuntimeException()));

		//when
		Observable<Conversation> actual = service.fetchAll();

		//then
		then(actual.toBlocking().getIterator().hasNext()).isFalse();
	}
}